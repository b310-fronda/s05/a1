package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface PostService {
    void createPost(String stringToken, Post post);


    Iterable<Post> getPosts();
    //                        post id, user credentials,   request body
    ResponseEntity updatePost(Long id, String stringToken, Post post);
    ResponseEntity deletePost(Long id, String stringToken);
    Iterable<Post> myPosts(String stringToken);

}
